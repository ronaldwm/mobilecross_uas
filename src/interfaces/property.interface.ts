
export interface Property{
    id_user : String;
    id_property : String;
    name : String;
    description : String;
    phone_number : String;
    loc_alt : any;
    loc_lng : any;
    date : String;
    address : String;
    id_images : String;
    img : String;
}