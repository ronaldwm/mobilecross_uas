import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera } from '@ionic-native/camera';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { NewsPage } from '../pages/news/news';
import { MapPage } from '../pages/map/map';
import { UserPage } from '../pages/user/user';
import { GooglePlus } from '@ionic-native/google-plus';
import { DbService } from '../services/db';
import { AddPropertyPage } from '../pages/add-property/add-property';
import { ViewVrPage } from '../pages/view-vr/view-vr';

import { AgmCoreModule } from '@agm/core';
import { MapsModalPage } from '../pages/maps-modal/maps-modal';
import { HttpClientModule } from '@angular/common/http';
import { PropertyService } from '../services/PropertyService';
import { CreatePropertyPage } from '../pages/create-property/create-property';
import { MapPagesChoosePage } from '../pages/map-pages-choose/map-pages-choose';
import { CallNumber } from '@ionic-native/call-number';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    MapPage,
    NewsPage,
    UserPage,
    AddPropertyPage,
    ViewVrPage,
    MapsModalPage,
    CreatePropertyPage,
    MapPagesChoosePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    NewsPage,
    MapPage,
    UserPage,
    AddPropertyPage,
    ViewVrPage,
    MapsModalPage,
    CreatePropertyPage,
    MapPagesChoosePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera,
    DbService,
    GooglePlus,
    PropertyService,
    CallNumber
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
