import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Property } from "../interfaces/property.interface";
import { PropertyService } from './PropertyService';

@Injectable()
export class DbService {


    constructor(private http: HttpClient) { }

    getUser(data){
        return new Promise((resolve,reject)=>{
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            this.http.post("http://sadess.store/propertee_API/getUser.php",data).subscribe(data =>{
                resolve(data);
            }, err => {
                console.log("err");
                reject(err)
            });
        })
    }

    getProperty() {
        return new Promise((resolve, reject) => {
            this.http.get<Property[]>("http://sadess.store/propertee_API/getAllProperty.php").subscribe(data => {
                console.log(data);
                resolve(data);
            }), err => {
                reject(err);
            }
        })
    }

    addUser(data) {
        return new Promise((resolve, reject) => {
            this.http.post("http://sadess.store/propertee_API/addAccount.php", JSON.stringify(data)).subscribe(data => {
                resolve(data);
            }, err => {
                reject(err);
            });
        })
    }

    getVr(data) {

        return new Promise((resolve, reject) => {
            let params = new HttpParams().set('id_vr', data);
            this.http.get("http://sadess.store/propertee_API/getAllPropertyImageVR.php", {params: params}).subscribe(data => {
                resolve(data);
            }, err => {
                reject(err);
            });
        });
    }


    // addUrl = "http://sadess.store/img_property_test/upload.php";
    // addImage(test) {
    //     return new Promise((resolve, reject) => {
    //         this.http.post(this.addUrl, JSON.stringify(test))
    //         .subscribe(data => {
    //           resolve(data);
    //         }, err => {
    //           reject(err);
    //         });
    //       });
    // }

    // getUrl = "http://sadess.store/img_property_test/get.php";
    // getImages(){
    //     return new Promise((resolve, reject) => {
    //         this.http.get(this.getUrl).subscribe(data => {
    //             resolve(data);
    //         }, err => {
    //             reject(err);
    //         });
    //     });
    // }
}
