import { Property } from "../interfaces/property.interface";

export class PropertyService{
    properties : Property[] = [];
    lat : any;
    lang : any;

    setLang(lang :any){
        this.lang = lang;
    }

    setlat(lat:any){
        this.lat = lat;
    }

    getLat(){
        return this.lat;
    }

    getlang(){
        return this.lang;
    }

    seedProperties(properties : Property[]){
        this.properties = properties;
    }

    getProperty(){
        return this.properties;
    }
}