import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the ViewVrPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-view-vr',
  templateUrl: 'view-vr.html',
})
export class ViewVrPage {
  vrImage: any;
  img_vr: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private domSanitizer: DomSanitizer) {
    this.vrImage = navParams.get('vr');
    this.img_vr = "url('" + this.vrImage.image_vr + "')";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewVrPage');
  }

  getImage() {
    return this.domSanitizer.bypassSecurityTrustUrl(this.vrImage.image_vr);
  }

}
