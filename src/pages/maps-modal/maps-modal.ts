import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { PropertyService } from '../../services/PropertyService';
import { DbService } from '../../services/db';
import { ViewVrPage } from '../view-vr/view-vr';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the MapsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-maps-modal',
  templateUrl: 'maps-modal.html',
})
export class MapsModalPage {

  properties: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl : ViewController, 
    private propertyService : PropertyService, 
    private loadingCtrl: LoadingController, 
    private alertCtrl: AlertController, 
    private dbService: DbService,
    private callNumber: CallNumber) {

    this.properties = navParams.get('key');
    console.log(this.properties);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapsModalPage');
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  call(nmbr) {
    this.callNumber.isCallSupported().then((response) => {
      if(response == "true") {

        this.callNumber.callNumber(nmbr, true);

      } else {

        let alert = this.alertCtrl.create({
          title: 'Sorry',
          subTitle: 'This device does not support call',
          buttons: ['Ok']
        });
        alert.present();
        
      }
    }).catch((err) => {
      console.log(err);
    });
  }

  bookMark() {

  }

  viewVr(key) {
    if(key) {

      let loading = this.loadingCtrl.create({
        content: 'Please wait..'
      });

      console.log(key);

      loading.present();

      this.dbService.getVr(key).then((data) => {
        console.log(data);
        loading.dismiss();
        this.navCtrl.push(ViewVrPage, {vr: data});
      })
      .catch((err) => {
        console.log(err);
      });

    } else {
      let alert = this.alertCtrl.create({
        title: 'Sorry',
        subTitle: 'There is no VR for this property',
        buttons: ['Ok']
      });
      alert.present();
    }
  }

}
