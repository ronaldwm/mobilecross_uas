import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Property } from '../../interfaces/property.interface';
import { MapPagesChoosePage } from '../map-pages-choose/map-pages-choose';

/**
 * Generated class for the CreatePropertyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-create-property',
  templateUrl: 'create-property.html',
})
export class CreatePropertyPage {
  property : any = {loc_alt : "", loc_lng : "", name :"",desc:"",address:"",description:""};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  addProperty(f:NgForm){
    
  }

  choosePosition(){
    this.navCtrl.push(MapPagesChoosePage).then(()=>{
      console.log("a");
    });
  }

  ionViewWillEnter(){
    if(this.navParams.get('formData')!= null){
      this.property.loc_alt = this.navParams.get('formData').lat;
      this.property.loc_lng = this.navParams.get('formData').long;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatePropertyPage');
  }

}
