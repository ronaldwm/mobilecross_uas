import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import leaflet from 'leaflet';
import { MapsModalPage } from '../maps-modal/maps-modal';
import { DbService } from '../../services/db';
import { PropertyService } from '../../services/PropertyService';
import { Property } from '../../interfaces/property.interface';

/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  properties: Property[] = [];
  constructor(public db: DbService, private propertyService: PropertyService, public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) {
  }

  loadmap() {
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'www.tphangout.com',
      maxZoom: 18
    }).addTo(this.map);
  }

  ionSelected() {
    TabsPage.currPage = MapPage;
  }

  developerInputPos() {
    this.map.locate({
      setView: true,
      maxZoom: 18
    }).on('locationfound', (e) => {
      let markerGroup = leaflet.featureGroup();
      let marker: any = leaflet.marker([-6.172039657951596, 106.59510880708697], { draggable: 'true' }).on('dragend', (e) => {
        console.log(e.target.getLatLng());
      });
      markerGroup.addLayer(marker);
      this.map.addLayer(markerGroup);
    }).on('locationerror', (err) => {
      alert(err.message);
    })
  }

  ionViewDidEnter() {
    if (this.map != null) {
      let markerGroup2 = leaflet.featureGroup();
      this.db.getProperty().then((result: Property[]) => {
        this.propertyService.seedProperties(result);
        this.properties = this.propertyService.getProperty();

        var apartIcon = leaflet.Icon.extend({
          options: {
            iconSize: [70, 95],
            iconAnchor: [22, 94],
            popupAnchor: [-3, -76]
          }
        });
  
        var apaIcon = new apartIcon({ iconUrl: 'assets/imgs/apartment.png' });

        this.properties.forEach((element) => {
          let marker: any = leaflet.marker([element.loc_alt, element.loc_lng], {icon : apaIcon}).on('click', () => {
            this.modalCtrl.create(MapsModalPage, {key : element}).present();
          });;
          markerGroup2.addLayer(marker);
        });
        this.map.addLayer(markerGroup2);
      })

      //this.developerInputPos();
      var myIcon = leaflet.Icon.extend({
        options: {
          iconSize: [70, 95],
          iconAnchor: [22, 94],
          popupAnchor: [-3, -76]
        }
      });

      var meIcon = new myIcon({ iconUrl: 'assets/imgs/me-icon.png' });

      this.map.locate({
        setView: true,
        maxZoom: 18
      }).on('locationfound', (e) => {
        let markerGroup = leaflet.featureGroup();
        console.log(e.latitude);
        console.log(e.longitude);
        let marker: any = leaflet.marker([e.latitude, e.longitude], { icon: meIcon }).on('click', () => {
          this.modalCtrl.create(MapsModalPage);
        });

        markerGroup.addLayer(marker);
        this.map.addLayer(markerGroup);
      }).on('locationerror', (err) => {
        alert(err.message);
      })

      // let long,lat;
      // long = 106.594933;
      // lat = -6.1739353

      // let marker: any = leaflet.marker([lat,long]);

      // let markerGroup = leaflet.featureGroup();
      // markerGroup.addLayer(marker);
      // this.map.addLayer(markerGroup);
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MapPage');
    this.loadmap();
  }

}
