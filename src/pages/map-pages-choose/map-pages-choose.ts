import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DbService } from '../../services/db';
import { Property } from '../../interfaces/property.interface';
import { PropertyService } from '../../services/PropertyService';
import leaflet from 'leaflet';
import { CreatePropertyPage } from '../create-property/create-property';

/**
 * Generated class for the MapPagesChoosePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-map-pages-choose',
  templateUrl: 'map-pages-choose.html',
})
export class MapPagesChoosePage {
  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  long : any;
  lat : any;
  properties: Property[] = [];
  constructor(public db: DbService , private propertyService: PropertyService, public navCtrl: NavController, public navParams: NavParams) {
    
  }

  developerInputPos() {
    let markerGroup = leaflet.featureGroup();
    let marker: any = leaflet.marker([-6.172039657951596, 106.59510880708697], { draggable: 'true' }).on('dragend', (e) => {
      this.long = e.target.getLatLng().lng;
      this.lat = e.target.getLatLng().lat;
    });

    this.map.on('click',(e)=>{
      var lat = this.lat = (e.latlng.lat);
      var lng = this.long = (e.latlng.lng);
      var newLatLng = new leaflet.LatLng(lat,lng);
      marker.setLatLng(newLatLng);
    
    })
    markerGroup.addLayer(marker);
    this.map.addLayer(markerGroup);
  }
  
  loadmap() {
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attributions: 'www.tphangout.com',
      maxZoom: 18
    }).addTo(this.map);
    
    this.developerInputPos();

  }

  setLoc(){
    //var callback = this.navParams.get("callback");
    var x = {"long" : this.long, "lat" : this.lat};
    this.propertyService.setLang(this.long);
    this.propertyService.setlat(this.lat);
    this.navCtrl.getPrevious().data.formData = {lat : this.lat, long : this.long};
    this.navCtrl.pop();
  }


  ionViewDidLoad() {
    this.loadmap();
    console.log('ionViewDidLoad MapPagesChoosePage');
  }

}
