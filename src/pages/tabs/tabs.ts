import { Component, AfterViewChecked, AfterViewInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';
import { NewsPage } from '../news/news';
import { MapPage } from '../map/map';
import { UserPage } from '../user/user';

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage implements  AfterViewChecked, AfterViewInit {
  @ViewChild('mainTabs') myTabs: Tabs;
  newsPage = NewsPage;
  mapPage = MapPage;
  userPage = UserPage;

  static currPage :any;


  public changeUI(){
    let tab:any = this.myTabs.getSelected();
  }

  ngAfterViewInit() {
 
  }

  ngAfterViewChecked() {

  }


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    console.log("a");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
