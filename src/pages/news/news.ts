import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DbService } from '../../services/db';
import { PropertyService } from '../../services/PropertyService';
import { ViewVrPage } from '../view-vr/view-vr';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  properties: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private dbService: DbService, 
    private propertyService: PropertyService, 
    private alertCtrl: AlertController, 
    private loadingCtrl: LoadingController,
    private callNumber: CallNumber) {

    TabsPage.currPage = NewsPage;
  }

  ionViewDidLoad() {
    this.properties = [];
    console.log('ionViewDidLoad NewsPage');
    this.dbService.getProperty().then((data) => {
      this.properties = data;

      console.log(this.properties);

      this.propertyService.seedProperties(this.properties);
    })
    .catch((err) => {
      console.log(err);
    })

  }

  call(nmbr) {
    this.callNumber.isCallSupported().then((response) => {
      if(response == "true") {

        this.callNumber.callNumber(nmbr, true);

      } else {

        let alert = this.alertCtrl.create({
          title: 'Sorry',
          subTitle: 'This device does not support call',
          buttons: ['Ok']
        });
        alert.present();

      }
    }).catch((err) => {
      console.log(err);
    });
  }

  bookMark() {
    
  }

  viewVr(key) {
    if(key) {

      let loading = this.loadingCtrl.create({
        content: 'Please wait..'
      });

      console.log(key);

      loading.present();

      this.dbService.getVr(key).then((data) => {
        console.log(data);
        loading.dismiss();
        this.navCtrl.push(ViewVrPage, {vr: data});
      })
      .catch((err) => {
        console.log(err);
      });

    } else {
      let alert = this.alertCtrl.create({
        title: 'Sorry',
        subTitle: 'There is no VR for this property',
        buttons: ['Ok']
      });
      alert.present();
    }
  }

}
