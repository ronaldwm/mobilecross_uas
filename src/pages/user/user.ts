import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ActionSheetController, Toast, ModalController  } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { DbService } from '../../services/db';
import { NgForm } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { identifierModuleUrl } from '@angular/compiler';
import { CreatePropertyPage } from '../create-property/create-property';

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  person = {};
  useGoogle:boolean;
  userLogin = {name :"",email : "",privilage : "", url : ""}
  privilage: string;
  user = {name : "", email : "", password : "", phone_nmbr : "", address : "", img : "", privilage : "user"};
  picture: any;
  register: boolean;
  loggedIn: boolean;
  login: boolean;
  loginVar = {email : "", password : ""};
  photos: any;

  constructor(public modalCtrl: ModalController,public camera : Camera,public actionSheetCtrl : ActionSheetController,public toastCtrl : ToastController,public db: DbService,public navCtrl: NavController, public navParams: NavParams, private googlePlus: GooglePlus) {
    this.loggedIn = false;
    this.register = false;
    this.useGoogle = false;
    this.login=false;
  }

  logout(){
    this.loggedIn = false;
    this.register = false;
    this.useGoogle = false;
    this.userLogin = {name :"",email : "",privilage : "", url : ""};
    this.user = {name : "", email : "", password : "", phone_nmbr : "", address : "", img : "", privilage : ""};
  
  }

  registerBtn(){
    //this.user.img = this.picture; mungkin ntar fotony gini

  }

  private optionsCamera: CameraOptions = {
    quality: 100,
    targetWidth: 600,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  private optionsGallery: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  changeLoginState() {
    this.googlePlus.login({}).then(
      res => {
        this.user.name = res.displayName;
        this.user.email = res.email;
        this.user.privilage = "user";
        this.user.img = this.userLogin.url;
        this.db.addUser(this.user).then(e=>{

        }).catch(e=>{

        })
      })
      .catch(err => {
        alert("error");
      });
  }

  regist() {
    this.register = (this.register) ? false : true;
  }

  loginUser(loginForm: NgForm){
    this.db.getUser({"id_users" : this.loginVar.email}).then((e:any)=>{
        if(e==null){
          this.toastCtrl.create({
            message : "Id Atau Password Salah",
            duration : 1000
          }).present();
        }else{
          if (e.password == this.loginVar.password){
              this.loggedIn = true;
              this.login = this.register = false;
              this.userLogin.email = e.email;
              this.userLogin.name = e.name;
          }else{
            this.toastCtrl.create({
              message : "Id Atau Password Salah",
              duration : 1000
            }).present();
          }
        }
    })
  }

  loginStates(){
    this.login = (this.login) ? false : true;
  }

  addUser(f: NgForm) {
   this.db.getUser({"id_users" : this.user.email}).then(e=>{
     if(e == null){
        this.db.addUser(this.user).then(e=>{
          this.register = this.login = this.loggedIn = false;
          this.toastCtrl.create({
            message : "Sukses Mendaftar",
            duration : 1000
          }).present();
        }).catch(e => {
          this.register = this.login = this.loggedIn = false;
          this.toastCtrl.create({
            message : "Sukses Mendaftar",
            duration : 1000
          }).present(); 
        })
     }else{
       this.toastCtrl.create({
         message : "Email sudah terdaftar",
         duration : 1000
       }).present();
       }
   });
  }

  get_photo(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Get From',
      buttons: [
        {
          text: 'Get From Camera',
          handler: () => {
            this.camera.getPicture(this.optionsCamera).then((imageData) => {
              let base64image = imageData;
              this.photos = base64image;
            })
          }
        },
        {
          text: 'Get From Gallery',
          handler: () => {
            this.camera.getPicture(this.optionsGallery).then((imageData) => {
              let base64image = 'data:image/jpeg;base64,' + imageData;
              this.photos = base64image;
            })
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
  }

  addProperties(){
    this.modalCtrl.create(CreatePropertyPage,{"id_users" : this.user.email}).present();
  }

  ionViewDidEnter(){
    this.login = this.register = this.loggedIn = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

}
