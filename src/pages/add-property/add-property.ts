import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NgForm } from '@angular/forms';

/**
 * Generated class for the AddPropertyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage()
@Component({
  selector: 'page-add-property',
  templateUrl: 'add-property.html',
})
export class AddPropertyPage {

  photos: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPropertyPage');
  }

  addProperty(f: NgForm) {
    let property = {
      'property_name' : f.value.property_name,
      'description' : f.value.description,
      'phone_number' : f.value.phone_number,
      'address' : f.value.address
    }
  }

  private optionsCamera: CameraOptions = {
    quality: 100,
    targetWidth: 600,
    sourceType: this.camera.PictureSourceType.CAMERA,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  private optionsGallery: CameraOptions = {
    quality: 100,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  

  camera_photo() {
    this.camera.getPicture(this.optionsCamera).then((imageData) => {
      let base64image = imageData;
      this.photos.push(base64image);
    })
  }

  gallery_photo() {
    this.camera.getPicture(this.optionsGallery).then((imageData) => {
      let base64image = 'data:image/jpeg;base64,' + imageData;
      this.photos.push(base64image);
    })
  }

}
